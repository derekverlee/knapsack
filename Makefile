
version=0.1.0-SNAPSHOT
jar=target/ao-challenge-${version}-standalone.jar
app=rxdollies.jar
sorted_tmp=target/test-expected-out-sorted.tmp


all: fulltest app

app: ${app}

unittest:
	lein test

fulltest: unittest functional_test

${app}: ${jar}
	cp -f ${jar} ${app}

${jar}:
	lein uberjar

functional_test: ${app}
	(mkdir -p target; \
	sort test/resources/challange-provided-test.expected-out > ${sorted_tmp}; \
	java -jar ${app} < \
	test/resources/challange-provided-test.in | sort | \
	diff - ${sorted_tmp}) && \
	(mkdir -p target; \
	sort test/resources/test2.expected-out > ${sorted_tmp}; \
	java -jar ${app} < \
	test/resources/test2.in | sort | \
	diff - ${sorted_tmp} )\
	&& printf "\n\nGOOD" || printf "\n\nFAIL"


clean:
	lein clean
	rm -f ${app}
	rm -f ${sorted_tmp}
