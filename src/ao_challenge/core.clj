(ns ao-challenge.core
    (:gen-class)
    (:require clojure.contrib.def)
    (:require clojure.math.combinatorics))

(use 'clojure.contrib.def)
(use 'clojure.math.combinatorics)
(use '[clojure.string :only (split-lines split join)])

(defrecord Doll [name weight value])

(defn parse-doll [input]
  (let [fields (split input #"\s+")]
    (if (= 3 (count fields))
      (try
        (let [
              name (first fields)
              weight (Integer/parseInt (second fields))
              value (Integer/parseInt (nth fields 2))
            ]
          (->Doll name weight value))
        (catch NumberFormatException ex)))))


(defn find-max-weight [input]
  (let [match (second (re-find #"max weight: (\d+)" input))]
    (if match
      (try
        (Integer/parseInt match)
        (catch NumberFormatException ex -1))
      0)))

(defn input-handler []
  (let [input (slurp *in*)]
    [
     (find-max-weight input)
     (set (filter identity (map parse-doll (split-lines input))))
     ]))

(defn print-doll [doll]
  (format "%-8s %4d  %4d" (:name doll) (:weight doll) (:value doll)))

(defn output-handler [dolls]
  (str "packed dolls:" \newline \newline
       "name    weight value" \newline
       (join \newline
             (map print-doll dolls))))

;; Solver

(defnk solver-argument-validator [:max-weight nil :dolls []]
  "A helper function for solver"
  (if (or (not (number? max-weight)) (neg? max-weight))
    (throw (java.lang.IllegalArgumentException.
            "a non-negitive max-weight is required")))
  (if (not (coll? dolls))
    (throw (java.lang.IllegalArgumentException.
            "dolls must be a collection"))))


(defn knapsack [limit items]
  "Solve a knapsack problem, returning [value [item item ...]]"
  (if (= 0 (count items))
    [0 items]
    (let [it (first items)
          rst (rest items)]
      (if (> (:weight it) limit)
        (knapsack limit rst)
        (let
            [with-item-weight (knapsack (- limit (:weight it)) rst)
             without-item (knapsack limit rst)
             with-item [(+ (:value it) (first with-item-weight))
                           (conj (second with-item-weight) it)]
             ]
          (max-key first with-item without-item))))))


(defnk solver [:max-weight nil :dolls []]
  "Solve the doll problem for a list of dolls"
  (solver-argument-validator :max-weight max-weight :dolls dolls)
  (set (second (knapsack max-weight dolls))))


(defn -main [& args]
  "I'm the main function, directing command line execution"
  (let [[max-weight dolls] (input-handler)]
    (println (output-handler (solver :max-weight max-weight :dolls dolls)))))
