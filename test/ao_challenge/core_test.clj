(ns ao-challenge.core-test
  (:use clojure.test
        ao-challenge.core))

(deftest compiler-test
  (testing "Compiler"
    (is (= 1 1))))

(deftest doll-test
  (testing "Dolls"
    (testing "are basically just immutable objects that store some data by name")
    (let [expected-name "test123" expected-weight 379 expected-value 782
      testdoll (->Doll expected-name expected-weight expected-value)]
      (is (= (:name testdoll) expected-name))
      (is (= (:weight testdoll) expected-weight))
      (is (= (:value testdoll) expected-value)))))

(deftest solver-test
  (testing "Solver"
    (testing "parameter validation"
      (is (thrown? IllegalArgumentException
                   (solver :max-weight -1 :dolls [])))
      (is (thrown? IllegalArgumentException
                   (solver :dolls [])))
      (is (thrown? IllegalArgumentException
                   (solver)))
      (is (thrown? IllegalArgumentException
                   (solver :max-weight 100
                           :dolls 'this-symbol-isnt-a-collection))))

    (testing "trivial cases"
      (is (= #{}
             (solver :max-weight 0 :dolls [])))
      (let [expected-name "test123" expected-weight 379 expected-value 782
            testdoll (->Doll expected-name expected-weight expected-value)]
        (is (= #{testdoll}
               (solver :max-weight 20000 :dolls #{testdoll})
               ))
        (is (= #{}
               (solver :max-weight 10 :dolls [testdoll])))))

    (testing "some very easy cases"
      (let [
            doll1 (->Doll "doll1" 25 100)
            doll2 (->Doll "doll2" 50  50)
            doll3 (->Doll "doll3" 25 105)
            doll4 (->Doll "doll4" 1  104)
            doll5 (->Doll "doll5" 75  33)
            ]
      (is (= #{doll3}
             (solver :max-weight 26 :dolls [doll1 doll3])))
      (is (= #{doll1 doll3}
             (solver :max-weight 50 :dolls #{doll1 doll3})))
      (is (= #{doll1 doll2 doll3 doll4 doll5}
             (solver :max-weight 1000 :dolls [doll1 doll2 doll3 doll4 doll5])))
      (is (= #{doll1 doll2 doll3 doll4}
             (solver :max-weight 101 :dolls [doll1 doll2 doll3 doll4 doll5])))
      (is (= #{doll1 doll3 doll4}
             (solver :max-weight 100 :dolls [doll1 doll2 doll3 doll4 doll5])))
      (is (= #{doll3 doll4}
             (solver :max-weight 50 :dolls [doll1 doll2 doll3 doll4 doll5])))
      (is (= #{doll3}
             (solver :max-weight 25 :dolls [doll1 doll2 doll3 doll4 doll5])))
      (is (= #{doll4}
             (solver :max-weight 5 :dolls [doll1 doll2 doll3 doll4 doll5])))))))


(def test-input1 "
max weight: 400

available dolls:

name    weight value
testing 155    999
")


(def test-input2 "

max weight: 29012

available dolls:

name    weight value
jake    42     0
luke    99     1
sally   101    10000
michael 103    1444
louis   102050 17147

")


(def test-input3
"max weight: 0

available dolls:

name    weight value")


(def test-input4
"max weight: 92

available dolls:

name    weight value
sandmitch 1 2 wut?
JUNE   7029309			1020992")


(deftest input-handler-test
  (testing "input-handler"
    (are [input parsed]
         (with-in-str input (is (= parsed (input-handler))))
         test-input1 [400
                      #{(->Doll "testing" 155 999)}]
         test-input2 [29012
                      #{(->Doll "jake"    42  0)
                        (->Doll "luke"    99 1)
                        (->Doll "sally"   101 10000)
                        (->Doll "michael" 103 1444)
                        (->Doll "louis"   102050 17147)}]
         test-input3 [0 #{}]
         test-input4 [92 #{(->Doll "JUNE" 7029309 1020992)}])))


(def expected-out1
"packed dolls:

name    weight value
sally       4    50")


(def expected-out2
"packed dolls:

name    weight value
")

(def expected-out3
"packed dolls:

name    weight value
sally     999     1")


(deftest output-handler-test
  (testing "output-handler"
    (testing "uses correct formatting"
      (is (= expected-out1 (output-handler #{(->Doll "sally" 4 50)})))
      (is (= expected-out2 (output-handler #{})))
      (is (= expected-out3 (output-handler #{(->Doll "sally" 999 1)}))))
    (testing "output is parseable as input (without the max-weight)"
      (let [dolls #{ (->Doll "dorthy" 50 160)
                     (->Doll "candice" 153 200)
                     (->Doll "anthony" 13 35)
                     (->Doll "luke" 9 150) }]
        (is (= dolls (second (with-in-str (output-handler dolls) 
                               (input-handler)))))))))
