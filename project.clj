(defproject ao-challenge "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.4.0"]
                 [org.clojure/math.combinatorics "0.0.3"]
                 [org.clojure/clojure-contrib "1.2.0"]
                 ]
  :aot [ao-challenge.core]
  :main ao-challenge.core)
